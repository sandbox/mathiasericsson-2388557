<?php
/**
* Implementation of hook_rules_action_info().
 */

function performance_insight_rules_action_info() {
  return array(
    'performance_insight_rules_action_run_performance_check' => array(
      'label' => t('Run performance insight on node'),
      'group' => t('Performance'),
      'parameter' => array(
        'nid' => array(
          'type' => 'node',
          'label' => t('nid'),
          'description' => t('The nid of the node you want to run performance insight'),
        ),
      ),
    ),
    'performance_insight_rules_action_run_performance_check_on_all_nodes' => array(
      'label' => t('Run performance insight on all nodes'),
      'group' => t('Performance'),
    ),
  );
}

/*
* Callback function for rules action hook
* */
function performance_insight_rules_action_run_performance_check($node){

  $performance_insight_content_type_url_field = variable_get('performance_insight_content_type_url_field', '');

  if(isset($performance_insight_content_type_url_field)){
    $url =  $node->{$performance_insight_content_type_url_field}['und'][0]['value'];
    $nid = $node->nid;

    $insight = google_insight_fetch_data($url, $nid);

    if(isset($insight)){
      $insight->save();
      drupal_set_message(t('Added insight to DB for node with ID = ') . $nid, 'status');
    }
  }
}

/*
 * Runs performance check on all node types of the specified content type
 * */

function performance_insight_rules_action_run_performance_check_on_all_nodes(){

  $performance_insight_content_type = variable_get('performance_insight_content_type', '');

  if(isset($performance_insight_content_type)){

    $query = new EntityFieldQuery();
    $query
      ->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', $performance_insight_content_type);
    $result = $query->execute();

    if(isset($result)){

      foreach($result['node'] as $result_node){
        $node = node_load($result_node->nid);

        if(isset($node))
          performance_insight_rules_action_run_performance_check($node);
      }
    }
  }

}
