<?php

/**
 * Implement admin forms
 *
 * @param $form
 * @param $form_state
 */

function performance_insight_form() {
  $form = array();

  $form['title'] = array(
    '#type' => 'item',
    '#title' => t('<h2>Insights admin settings</h2>'),
  );
  $form['default_settings']['description'] = array(
    '#type' => 'item',
    '#description' => t('This page contains the basic settings needed to set up the insights module'),
  );


  $default_performance_insight_google_api_key = variable_get('performance_insight_google_api_key', '');
  $default_performance_insight_content_type = variable_get('performance_insight_content_type', '');
  $default_performance_insight_content_type_url_field = variable_get('performance_insight_content_type_url_field', '');
  $default_performance_insight_acceptable_rule_impact = variable_get('performance_insight_acceptable_rule_impact', '2');


  $form['default_settings']['performance_insight_google_api_key'] = array(
    '#type' => 'textfield',
    '#name' => 'performance_insight_google_api_key',
    '#size' => 20,
    '#title' => t('Enter your Google API key'),
    '#default_value' => $default_performance_insight_google_api_key,
    '#description' => t('<p>This is the key you get from Google when you register a dev account.</p>'),

  );

  $content_types = node_type_get_names();

  $form['default_settings']['performance_insight_content_type'] = array(
    '#type' => 'radios',
    '#name' => 'performance_insight_content_type',
    '#size' => 20,
    '#title' => t('Select the content type that you want to get insight on'),
    '#default_value' =>  $default_performance_insight_content_type,
    '#description' => t('<p>This is the machine name of the content type you want to get insight from, note you need to configure a field with a URL on that content</p>'),
    '#options' => $content_types

  );


  //Get all fields
  $fields = field_info_field_map();
  $fields = array_keys($fields);

  //Baking field array to send in the value as both key and value in array
  $fields_new = array();
  foreach($fields as $field_key){
    $fields_new[$field_key] = $field_key;
  }

  $form['default_settings']['performance_insight_content_type_url_field'] = array(
      '#type' => 'radios',
      '#name' => 'performance_insight_content_type_url_field',
      '#size' => 20,
      '#title' => t('Enter the machine name of the URL field you want to check'),
      '#default_value' =>  $default_performance_insight_content_type_url_field,
      '#description' => t('<p>This is the machine name of the content type you want to get insight from, note you need to configure a field with a URL on that content</p>'),
      '#options' => $fields_new
    );

  $form['default_settings']['performance_insight_acceptable_rule_impact'] = array(
    '#type' => 'textfield',
    '#name' => 'performance_insight_acceptable_rule_impact',
    '#size' => 20,
    '#title' => t('Enter the acceptable level of rule impact, everything equal or less to this value will count as a successful check'),
    '#default_value' =>  $default_performance_insight_acceptable_rule_impact,
    '#description' => t('<p>This is the machine name of the content type you want to get insight from, note you need to configure a field with a URL on that content</p>'),

  );

  $form = system_settings_form($form);
  $form['#submit'][] = 'performance_insight_form';

  return $form;
}


/**
 * Implement custom form submit
 *
 * @param $form
 * @param $form_state
 */
function performance_insight_variable_settings_form_submit($form, &$form_state) {


  $old_value = variable_get('performance_insight_google_api_key', '');
  $new_value = $form_state['default_settings']['performance_insight_google_api_key'];
  if ($old_value != $new_value) {
    variable_set('performance_insight_google_api_key',$new_value);
  }

  $old_value = variable_get('performance_insight_content_type', '');
  $new_value = $form_state['default_settings']['performance_insight_content_type'];
  if ($old_value != $new_value) {
    variable_set('performance_insight_content_type',$new_value);
  }

  $old_value = variable_get('performance_insight_content_type_url_field', '');
  $new_value = $form_state['default_settings']['default_performance_insight_content_type_url_field'];
  if ($old_value != $new_value) {
    variable_set('default_performance_insight_content_type_url_field',$new_value);
  }

  $old_value = variable_get('performance_insight_acceptable_rule_impact', '');
  $new_value = $form_state['default_settings']['default_performance_insight_acceptable_rule_impact'];
  if ($old_value != $new_value) {
    variable_set('default_performance_insight_acceptable_rule_impact',$new_value);
  }
}