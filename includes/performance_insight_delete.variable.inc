<?php

/**
 * Implement admin forms
 *
 * @param $form
 * @param $form_state
 */

function performance_insight_form() {
  $form = array();

  $form['title'] = array(
    '#type' => 'item',
    '#title' => t('<h2>Delete performance insights from DB</h2>'),
  );
  $form['default_settings']['description'] = array(
    '#type' => 'item',
    '#description' => t('On this page you can delete insights from DB, be aware no data can be restored after this action'),
  );

  $form['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Delete all insights from DB'),
    '#weight' => 5,
    '#submit' => array('performance_insight_delete_data'),
  );

  return $form;
}
/**
 * Callback function for delete data button, it goes through and delete all content of type insight
 */
function performance_insight_delete_data()
{

 $result = db_select('insight_item', 'n')
    ->fields('n', array('id'))
    ->execute();

  $ids = array();

  while($record = $result->fetchAssoc()) {
    array_push($ids, $record['id']);
  }

  entity_delete_multiple('insight', $ids);
  dpm(t('All insights deleted'));

}